# La trigonométrie en première STI2D.


???+video "Cours - Les fonctions trigonométriques - Cosinus et Sinus"

	<center>
		[![texte](http://img.youtube.com/vi/U33kSUs0ZkA/0.jpg)](https://www.youtube.com/watch?v=U33kSUs0ZkA "Les fonctions trigonométriques - Cosinus et Sinus")
	</center>

???+video "Cours - Déterminer graphiquement si une fonction est périodique"

	<center>
		[![texte](http://img.youtube.com/vi/pdk_qQfbzQ4/0.jpg)](https://www.youtube.com/watch?v=pdk_qQfbzQ4 "Déterminer graphiquement si une fonction est périodique")
	</center>

???+video "Cours - Déterminer algébriquement si une fonction est périodique"

	<center>
		[![texte](http://img.youtube.com/vi/mJm3wVQeY14/0.jpg)](https://www.youtube.com/watch?v=mJm3wVQeY14 "Déterminer algébriquement si une fonction est périodique")
	</center>

???+ exercice "Exercice 1"

    1. Soit \(f\) la fonction définie par \(f(x)=\sin(2x)\). Montrer que \(f\) est \(\pi\)-périodique.
    2. Soit \(g\) la fonction définie par \(g(x)=\cos(2x)\). Montrer que \(g\) est \(\pi\)-périodique.
    3. Soit \(h\) la fonction définie par \(h(x)=\cos(\pi{}x)\). Montrer que \(h\) est \(2\)-périodique.
    4. Soit \(k\) la fonction définie par \(k(x)=\sin(2\pi{}x)\). Montrer que \(k\) est \(1\)-périodique.

    Pour la première question, il faut faire exactement comme dans la 3ème vidéo. Pour les trois autres questions, il faut s'en inspirer.

???+ exercice "Exercice 2"

    Pour chacune des fonctions, déterminer son amplitude \(A\), sa phase à l'origine \(\varphi\), sa pulsation \(\omega\), sa période \(T\) et sa fréquence \(f\) :

    1. \(f(t)=2\cos\left(2t\right)\)
    2. \(g(t)=4,5\sin\left(5t-\dfrac{\pi}{4}\right)\)
    3. \(h(t)=10\cos\left(0,5t+\dfrac{\pi}{3}\right)\)
    4. \(l(t)=\sqrt{2}\cos\left(\pi{}t+2\right)\)

???+ exercice "Exercice 3"

    Les courbes ci-dessous représentent des fonction sinusoïdales du type \(f(t)=A\cos(\omega{}t+\varphi)\). Pour chacune d'elle, déterminer son amplitude \(A\), sa période \(T\), sa pulsation \(\omega\), sa phase à l'origine \(\varphi\) et sa fréquence \(f\) :

    1. <center>![Courbe 1](./images/courbe1.png)</center>
    2. <center>![Courbe 1](./images/courbe2.png)</center>
    3. <center>![Courbe 1](./images/courbe3.png)</center>
    4. <center>![Courbe 1](./images/courbe4.png)</center>
