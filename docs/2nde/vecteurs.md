---
level: page1
---

# Les vecteurs en seconde

Voici ce qu'il faut savoir sur les vecteurs en 2nde :

## 1. C'est quoi un vecteur ?

???+video "Cours - C'est quoi un vecteur ?"

	<center>
		[![texte](http://img.youtube.com/vi/W0dfX4sDq5g/0.jpg)](https://www.youtube.com/watch?v=W0dfX4sDq5g "C'est quoi un vecteur")
	</center>

## 2. Déterminer graphiquement les coordonnées d'un vecteur

???+note "Remarque 1 : Les coordonnées d'un vecteur"

	Les coordonnées d'un point sont **toujours** notées en ligne, c'est-à-dire l'abscisse à gauche et l'ordonnée à droite, séparées par un « ; », par exemple \(A(-1;2)\).
	
	Les coordonnées d'un vecteur sont **souvent** notées en colonne, c'est-à-dire la première coordonnée en haut, et la deuxième en bas, par exemple \(\overrightarrow{u}\left(\begin{array}{c}-1\\2\end{array}\right)\).
	
	L'avantage est de pouvoir facilement voir si on parle d'un point ou d'un vecteur. Dans certains manuels, on trouve cependant parfois les coordonnées des vecteurs en ligne, par exemple \(\overrightarrow{u}(-1;2)\).

???+video "Cours - Déterminer graphiquement les coordonnées d'un vecteur"

	<center>
		[![texte](http://img.youtube.com/vi/yk_tETEQx6U/0.jpg)](https://www.youtube.com/watch?v=yk_tETEQx6U "Déterminer graphiquement les coordonnées d'un vecteur")
	</center>

???+exercice "Exercice 1"

	=== "Énoncé"
	
		Faire l'exercice 53 page 152 du manuel :
		
		<center>![Exercice 53 page 152](./images/Ex53p152.PNG)</center>
	
	=== "Solution"
	
		\(\overrightarrow{u}\left(\begin{array}{c}-1\\4\end{array}\right), \overrightarrow{v}\left(\begin{array}{c}2\\0\end{array}\right), \overrightarrow{w}\left(\begin{array}{c}-2\\-2\end{array}\right), \overrightarrow{r}\left(\begin{array}{c}0\\-4\end{array}\right), \overrightarrow{CD}\left(\begin{array}{c}1\\2\end{array}\right)\text{ et }\overrightarrow{KL}\left(\begin{array}{c}3\\-1\end{array}\right)\)


???+exercice "Exercice 2"

	=== "Énoncé"
	
		Lire les coordonnées des vecteurs \(\overrightarrow{r}\), \(\overrightarrow{s}\), \(\overrightarrow{t}\), \(\overrightarrow{u}\), \(\overrightarrow{v}\) et \(\overrightarrow{w}\) dans la base \(\left(\overrightarrow{i},\overrightarrow{j}\right)\) ci-dessous :
		
		<center>![Exercice 1 du poly](./images/Ex1-Poly.PNG)</center>
	
	=== "Solution"
	
		\(\overrightarrow{r}\left(\begin{array}{c}-4\\0\end{array}\right), \overrightarrow{s}\left(\begin{array}{c}0\\2\end{array}\right), \overrightarrow{t}\left(\begin{array}{c}8\\-2\end{array}\right), \overrightarrow{u}\left(\begin{array}{c}-1\\2\end{array}\right), \overrightarrow{v}\left(\begin{array}{c}1\\2\end{array}\right)\text{ et }\overrightarrow{w}\left(\begin{array}{c}-2\\-1\end{array}\right)\)

## 3. Déterminer par des calculs les coordonnées d'un vecteur

???+video "Cours - Déterminer par des calculs les coordonnées d'un vecteur"

	<center>
		[![texte](http://img.youtube.com/vi/El1M1s9pHhY/0.jpg)](https://www.youtube.com/watch?v=El1M1s9pHhY "Déterminer par des calculs les coordonnées d'un vecteur")
	</center>

???+exercice "Exercice 3"

	=== "Énoncé"
		
		Dans un repère, soient \(A(-1;2)\) et \(B(2;-3)\) deux points. Calculer les coordonnées de \(\overrightarrow{AB}\).
	
	=== "Solution"
	
		On utilise la formule vue dans la vidéo, en se rappelant bien qu'on fait toujours « arrivée - départ ». Ici, on fait donc les coordonnées de \(B\) (l'arrivée) moins celles de \(A\) (le départ).
		
		On a donc \(\overrightarrow{AB}\left(\begin{array}{c}x_B-x_A\\y_B-y_A\end{array}\right)\) donc \(\overrightarrow{AB}\left(\begin{array}{c}2-(-1)\\-3-2\end{array}\right)\) donc \(\overrightarrow{AB}\left(\begin{array}{c}2+1\\-3-2\end{array}\right)\) donc \(\overrightarrow{AB}\left(\begin{array}{c}3\\-5\end{array}\right)\).
		
		Il faut bien comprendre qu'un vecteur représente un déplacement, ici, allant du point \(A\) vers le point \(B\). En abscisse, on se déplace de \(-1\) vers \(2\), donc de \(3\). En ordonnée, on se déplace de \(2\) vers \(-3\), donc on recule de \(5\), donc on se déplace de \(-5\). C'est bien ce qu'on a trouvé avec les calculs.

???+exercice "Exercice 4"

	=== "Énoncé"
		
		Dans un repère, soient \(M(-1;-2)\) et \(P(3;-1)\) deux points. Calculer les coordonnées de \(\overrightarrow{PM}\).
	
	=== "Solution"
	
		On utilise la formule vue dans la vidéo, en se rappelant bien qu'on fait toujours « arrivée - départ ». Ici, on fait donc les coordonnées de \(M\) (l'arrivée) moins celles de \(P\) (le départ). Attention, les points ne sont pas toujours donnés « dans l'ordre » dans l'énoncé.
		
		On a donc \(\overrightarrow{PM}\left(\begin{array}{c}x_M-x_P\\y_M-y_P\end{array}\right)\) donc \(\overrightarrow{PM}\left(\begin{array}{c}-1-3\\-2-(-1)\end{array}\right)\) donc \(\overrightarrow{PM}\left(\begin{array}{c}-1-3\\-2+1\end{array}\right)\) donc \(\overrightarrow{PM}\left(\begin{array}{c}-4\\-1\end{array}\right)\).
		
		Il faut bien comprendre qu'un vecteur représente un déplacement, ici, allant du point \(P\) vers le point \(M\). En abscisse, on se déplace de \(3\) vers \(-1\), donc on recule de \(4\), donc on se déplace de \(-4\). En ordonnée, on se déplace de \(-1\) vers \(-2\), donc on recule de \(1\), donc on se déplace de \(-1\). C'est bien ce qu'on a trouvé avec les calculs.
		
???+note "Cours - Égalité de deux vecteurs"

	Deux vecteurs sont égaux si, et seulement si ils ont les mêmes coordonnées.

???+exercice "Exercice 5"

	=== "Énoncé"
	
		Faire l'exercice 57 page 152 du manuel :
		
		<center>![Exercice 57 page 152](./images/Ex57p152.PNG)</center>
	
	=== "Solution"
	
		1. On a \(\overrightarrow{AB}\left(\begin{array}{c}x_B-x_A\\y_B-y_A\end{array}\right)\) donc \(\overrightarrow{AB}\left(\begin{array}{c}2-3\\-1-5\end{array}\right)\) donc \(\overrightarrow{AB}\left(\begin{array}{c}-1\\-6\end{array}\right)\).
		
			De même, on a \(\overrightarrow{DC}\left(\begin{array}{c}x_C-x_D\\y_C-y_D\end{array}\right)\) donc \(\overrightarrow{DC}\left(\begin{array}{c}-2-(-1)\\-4-2\end{array}\right)\) donc \(\overrightarrow{DC}\left(\begin{array}{c}-2+1\\-4-2\end{array}\right)\) donc \(\overrightarrow{DC}\left(\begin{array}{c}-1\\-6\end{array}\right)\).
			
		2. Les vecteurs \(\overrightarrow{AB}\) et \(\overrightarrow{DC}\) ont les mêmes coordonnées, donc \(\overrightarrow{AB}=\overrightarrow{DC}\).
		
			Il faut se souvenir qu'avoir deux vecteurs égaux est équivalent à avoir un parallélogramme. Il faut faire attention à l'ordre des lettres !
			
			Ici, on en déduit que \(ABCD\) est un parallélogramme.

???+exercice "Exercice 6"

	=== "Énoncé"
	
		Faire l'exercice 64 page 153 du manuel :
		
		<center>![Exercice 64 page 153](./images/Ex64p153.PNG)</center>
	
	=== "Solution"
	
		On a \(\overrightarrow{AB}\left(\begin{array}{c}x_B-x_A\\y_B-y_A\end{array}\right)\) donc \(\overrightarrow{AB}\left(\begin{array}{c}x_B-5\\y_B-2\end{array}\right)\).
		
		On sait que \(\overrightarrow{u}=\overrightarrow{AB}\) donc les deux vecteurs ont les **mêmes** coordonnées.
		
		On en déduit donc que \(x_B-5 = -2\) et que \(y_B-2=3\). On résout ces deux équations en ajoutant 5 dans la première, et en ajoutant 2 dans la deuxième.
		
		On trouve donc \(x_B=3\) et \(y_B=5\). Ainsi, \(B(3;5)\).

???+exercice "Exercice 7"

	=== "Énoncé"
	
		Faire l'exercice 65 page 153 du manuel :
		
		<center>![Exercice 65 page 153](./images/Ex65p153.PNG)</center>
	
	=== "Solution"
	
		- Pour que \(EFGH\) soit un parallélogramme, il faut que \(\overrightarrow{EF}=\overrightarrow{HG}\) (à nouveau, attention à l'ordre des lettres !).
		
		- On connait les coordonnées des points \(E\) et \(F\). On peut donc calculer cette de \(\overrightarrow{EF}\) :
		
			\(\overrightarrow{EF}\left(\begin{array}{c}x_F-x_E\\y_F-y_E\end{array}\right)\) donc \(\overrightarrow{EF}\left(\begin{array}{c}-3-2\\4-(-1)\end{array}\right)\) donc \(\overrightarrow{EF}\left(\begin{array}{c}-3-2\\4+1\end{array}\right)\) donc \(\overrightarrow{EF}\left(\begin{array}{c}-5\\5\end{array}\right)\).
		
		- On détermine les coordonnées de \(\overrightarrow{HG}\) en fonction de celles de \(H\) (qu'on ne connaît pas, puisqu'on les cherche) :
		
			\(\overrightarrow{HG}\left(\begin{array}{c}x_G-x_H\\y_G-y_H\end{array}\right)\) donc \(\overrightarrow{HG}\left(\begin{array}{c}1-x_H\\4-y_H\end{array}\right)\).
			
		- On sait que \(\overrightarrow{EF}=\overrightarrow{HG}\) donc les deux vecteurs ont les **mêmes** coordonnées. On a donc \(1-x_H=-5\) et \(4-y_H=5\).
		
			On résout ces deux équations, et on trouve que \(x_H=6\) et \(y_H=-1\).
			
			Ainsi, \(H(6;-1)\).


## 4. Déterminer si 2 vecteurs sont colinéaires

???+video "Cours - Déterminer si deux vecteurs sont colinéaires"

	<center>
		[![texte](http://img.youtube.com/vi/VHcfRVLAY4E/0.jpg)](https://www.youtube.com/watch?v=VHcfRVLAY4E "Déterminer si deux vecteurs sont colinéaires")
	</center>
    
    Dans cette vidéo, deux méthodes sont présentées pour savoir si deux vecteurs \(\overrightarrow{u}\) et \(\overrightarrow{v}\) sont colinéaires.
    Je vous demande uniquement de regarder la deuxième méthode, avec le **déterminant**.<br>
    Vous pouvez donc ne pas regarder la vidéo entre 2 minutes et 5 minutes 30.<br>
    Vous devez regarder le début qui explique ce que signifie _colinéaire_ et la fin, qui explique comment savoir si deux vecteurs sont colinéaires ou non avec la méthode du déterminant.


???+exercice "Exercice 8"
    
    === "Énoncé"
    
        Faire l'exercice 68 page 153, **questions 1 et 2 uniquement** :
		
		<center>![Exercice 65 page 153](./images/Ex68p153.PNG)</center>
        
    === "Solution"
    
        1. \(\det\left(\overrightarrow{u},\overrightarrow{v}\right)=(-2)\times{}(-4,5)-3\times{}3=9-9=0\) donc \(\overrightarrow{u}\) et \(\overrightarrow{v}\) sont colinéaires.
        2. \(\det\left(\overrightarrow{s},\overrightarrow{t}\right)=7\times{}4-14\times{}(-2)=28+28=56\neq{}0\) donc \(\overrightarrow{s}\) et \(\overrightarrow{t}\) ne sont pas colinéaires.
        3. \(\det\left(\overrightarrow{u},\overrightarrow{r}\right)=(-2)\times{}4,5-3\times{}3=-9-9=-18\neq{}0\) donc \(\overrightarrow{u}\) et \(\overrightarrow{r}\) ne sont pas colinéaires.
        4. \(\det\left(\overrightarrow{v},\overrightarrow{w}\right)=3\times{}12-(-8)\times{}(-4,5)=36-36=0\) donc \(\overrightarrow{v}\) et \(\overrightarrow{w}\) sont colinéaires.
        5. \(\det\left(\overrightarrow{s},\overrightarrow{m}\right)=7\times{}(-\frac{3}{7})-1\times{}(-2)=-3+2=-1\neq{}0\) donc \(\overrightarrow{s}\) et \(\overrightarrow{m}\) ne sont pas colinéaires.
        6. \(\det\left(\overrightarrow{m},\overrightarrow{t}\right)=1\times{}4-14\times{}\left(-\frac{3}{7}\right)=4+6=10\neq{}0\) donc \(\overrightarrow{m}\) et \(\overrightarrow{t}\) ne sont pas colinéaires.

## 5. Déterminer si 3 points sont alignés

???+video "Cours - Déterminer si 3 points sont alignés"

	<center>
		[![texte](http://img.youtube.com/vi/_H4h0jnKzEo/0.jpg)](https://www.youtube.com/watch?v=_H4h0jnKzEo "Déterminer si 3 points sont alignés")
	</center>

## 6. Déterminer si 2 droites sont parallèles

???+video "Cours - Déterminer si 2 droites sont parallèles"

	<center>
		[![texte](http://img.youtube.com/vi/hI3uWsUshko/0.jpg)](https://www.youtube.com/watch?v=hI3uWsUshko "Déterminer si 2 droites sont parallèles")
	</center>
