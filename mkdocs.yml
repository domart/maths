site_name: Cours de maths
site_url: https://mathematiques.gitlab.io/Cours/
site_dir: public

docs_dir: docs

nav:
    - Seconde:
        - 2nde/index.md
        - Les vecteurs: '2nde/vecteurs.md'
    - Première STI2D:
        - 1STI2D/index.md
        - Les vecteurs: '1STI2D/vecteurs1STI2D.md'
        - La trigonométrie: '1STI2D/trigo1STI2D.md'


theme:
    name: material
    font: false
    language: fr
    logo: images/logo.png
    favicon: images/logo.png
    features:
        - navigation.instant
        - navigation.tabs
        - navigation.expand
        - navigation.top
        #- toc.integrate # Permet d'avoir la nav à gauche et la tablea de matières à droite.
        - navigation.indexes # Permet d'associer une page directement à une section.
        #- navigation.sections
        - navigation.tabs.sticky   # Permet de toujours afficher la barre de nav en haut.
        - header.autohide
        - content.code.annotate
        - content.tabs.link

    palette:
        # Light mode
        - media: "(prefers-color-scheme: light)"
          scheme: default
          primary: teal
          accent: green
          toggle:
              icon: material/weather-night  #toggle-switch-off-outline
              name: Passer l'affichage en mode sombre

        # Dark mode
        - media: "(prefers-color-scheme: dark)"
          scheme: slate
          primary: grey
          accent: teal
          toggle:
              icon: material/weather-sunny   #toggle-switch
              name: Passer l'affichage en mode clair


markdown_extensions:
    - meta
    - abbr
    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - md_in_html                    # Pour mettre du markdown dans du HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - pymdownx.caret:               # Passage ^^souligné^^ ou en ^exposant^.
          insert: True
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.details              # ... qui peuvent se plier/déplier
    - pymdownx.highlight:           # Coloration syntaxique du code
          linenums: None
          auto_title: true
          auto_title_map:
              "Python": "🐍 Script Python"
              "Python Console Session": "🐍 Console Python"
              "Text Only": "📋 Texte"
              "E-mail": "📥 Entrée"
              "Text Output": "📤 Sortie"
              "Java": "☕ Code Java"
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
          custom_checkbox: false    # ...avec cases d'origine
          clickable_checkbox: true  # ...et cliquables.
    - pymdownx.superfences:         # Imbrication de blocs.
        custom_fences:              # mermaid permet de faire des diagrammes.
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.tabbed:
          alternate_style: true
    - pymdownx.betterem:            # Amélioration de la gestion de l'emphase (gras & italique).
          smart_enable: all
    - pymdownx.arithmatex:          # Formules en LaTeX dans le MD, converties en MathJax
          generic: true
    - toc:
          permalink: ⚓︎
          toc_depth: 4
    - pymdownx.emoji:               # Émojis  :boom:
          emoji_index: !!python/name:materialx.emoji.twemoji
          emoji_generator: !!python/name:materialx.emoji.to_svg

plugins:
    - search
    - awesome-pages   # Pour avoir accès à "awesome" (warning par exemple)
    - macros
    - encryptcontent:
        password_inventory:
            page1:
                user1: 'pwd1'
                user2: 'pwd2'
                user3: 'pwd3'
                user4: 'pwd4'
        title_prefix: '[Protégé] '
        summary: 'Accéder à une page protégée'
        decryption_failure_message: "Erreur sur le nom d'utilisateur ou sur le mot de passe"
        encryption_info_message: "Connecte toi pour accéder à cette page !"
        #button_class: 'md-search__icon'

extra_css:
    - css/monStyle.css    # style perso.
    - https://unpkg.com/mermaid@7.1.2/dist/mermaid.css     # Pour mermaid.

extra_javascript:
    - https://unpkg.com/mermaid@7.1.2/dist/mermaid.min.js  # Pour mermaid.
    ### Pour latex - Début.
    - xtra/javascripts/mathjax-config.js
    - https://polyfill.io/v3/polyfill.min.js?features=es6
    - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
    ### Pour latex - Fin.
